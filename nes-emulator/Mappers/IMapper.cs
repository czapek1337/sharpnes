﻿using System;

namespace nes_emulator.Mappers
{
    internal interface IMapper
    {
        bool CpuMapRead(ushort address, out uint mappedAddress);
        bool CpuMapWrite(ushort address, out uint mappedAddress, byte data = 0);

        bool PpuMapRead(ushort address, out uint mappedAddress);
        bool PpuMapWrite(ushort address, out uint mappedAddress, byte data = 0);

        void Reset();
    }
}
