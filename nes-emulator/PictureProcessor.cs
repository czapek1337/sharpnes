﻿using System;
using System.Runtime.InteropServices;
using PixelEngine;

namespace nes_emulator
{
    internal enum StatusRegister : byte
    {
        None = 0,
        SpriteOverflow = 1 << 5,
        SpriteZeroHit = 1 << 6,
        VerticalBlank = 1 << 7,
    }

    internal enum MaskRegister : byte
    {
        None = 0,
        GrayScale = 1 << 0,
        RenderBackgroundLeft = 1 << 1,
        RenderSpritesLeft = 1 << 2,
        RenderBackground = 1 << 3,
        RenderSprites = 1 << 4,
        EnhanceRed = 1 << 5,
        EnhanceGreen = 1 << 6,
        EnhanceBlue = 1 << 7,
    }
    
    internal enum ControlRegister : byte
    {
        None = 0,
        NametableX = 1 << 0,
        NametableY = 1 << 1,
        IncrementMode = 1 << 2,
        PatternSprite = 1 << 3,
        PatternBackground = 1 << 4,
        SpriteSize = 1 << 5,
        SlaveMode = 1 << 6,
        EnableNmi = 1 << 7,
    }

    internal enum LoopyRegister : ushort
    {
        None = 0,

        CoarseX1 = 1 << 0,
        CoarseX2 = 1 << 1,
        CoarseX3 = 1 << 2,
        CoarseX4 = 1 << 3,
        CoarseX5 = 1 << 4,

        CoarseY1 = 1 << 5,
        CoarseY2 = 1 << 6,
        CoarseY3 = 1 << 7,
        CoarseY4 = 1 << 8,
        CoarseY5 = 1 << 9,

        NametableX = 1 << 10,
        NametableY = 1 << 11,

        FineY1 = 1 << 12,
        FineY2 = 1 << 13,
        FineY3 = 1 << 14,
    }

    internal class PictureProcessor
    {
        private readonly Pixel[] _palScreen = new Pixel[64];

        private byte _statusRegister;
        private byte _maskRegister;
        private byte _controlRegister;
        private ushort _vramAddress;
        private ushort _tramAddress;

        private byte _fineX;
        private byte _addressLatch;
        private byte _ppuDataBuffer;

        private byte[,] _nameTables = new byte[2,1024];
        private byte[,] _patternTables = new byte[2,4096];
        private byte[]_palette = new byte[32];

        private Sprite[] _patternTableSprites = { new Sprite(128, 128), new Sprite(128, 128) };

        public int ScanLine { get; private set; }
        public int Cycle { get; private set; }
        public bool FrameComplete { get; set; }

        public Sprite Screen { get; } = new Sprite(256, 240);
        public Sprite[] NameTables { get; } = { new Sprite(256, 240), new Sprite(256, 240) };

        public Pixel GetColorFromPaletteRam(int palette, int pixel) => _palScreen[PpuRead((ushort) (0x3F00 + (palette << 2) + pixel)) & 0x3F];

        public Sprite GetPatternTable(int index)
        {
            for (var nTileY = 0; nTileY < 16; nTileY++)
            for (var nTileX = 0; nTileX < 16; nTileX++)
            for (var row = 0; row < 8; row++)
            {
                var nOffset = nTileY * 256 + nTileX * 16;

                var tileLsb = PpuRead((ushort) (index * 0x1000 + nOffset + row + 0x0000));
                var tileMsb = PpuRead((ushort) (index * 0x1000 + nOffset + row + 0x0008));

                for (var col = 0; col < 8; col++)
                {
                    var pixel = (tileLsb & 0x01) << 1 | (tileMsb & 0x01);

                    tileLsb >>= 1;
                    tileMsb >>= 1;

                    _patternTableSprites[index][nTileX * 8 + (7 - col), nTileY * 8 + row] = GetColorFromPaletteRam(0, pixel);
                }
            }

            return _patternTableSprites[index];
        }

        private Cartridge _cartridge;
        private readonly Random _rng = new Random();

        public PictureProcessor()
        {
            _palScreen = new[]
            {
                new Pixel(84, 84, 84), new Pixel(0, 30, 116), new Pixel(8, 16, 144), new Pixel(48, 0, 136),
                new Pixel(68, 0, 100), new Pixel(92, 0, 48), new Pixel(84, 4, 0), new Pixel(60, 24, 0),
                new Pixel(32, 42, 0), new Pixel(8, 58, 0), new Pixel(0, 64, 0), new Pixel(0, 60, 0),
                new Pixel(0, 50, 60), new Pixel(0, 0, 0), new Pixel(0, 0, 0), new Pixel(0, 0, 0),
                new Pixel(152, 150, 152), new Pixel(8, 76, 196), new Pixel(48, 50, 236), new Pixel(92, 30, 228),
                new Pixel(136, 20, 176), new Pixel(160, 20, 100), new Pixel(152, 34, 32), new Pixel(120, 60, 0),
                new Pixel(84, 90, 0), new Pixel(40, 114, 0), new Pixel(8, 124, 0), new Pixel(0, 118, 40),
                new Pixel(0, 102, 120), new Pixel(0, 0, 0), new Pixel(0, 0, 0), new Pixel(0, 0, 0),
                new Pixel(236, 238, 236), new Pixel(76, 154, 236), new Pixel(120, 124, 236), new Pixel(176, 98, 236),
                new Pixel(228, 84, 236), new Pixel(236, 88, 180), new Pixel(236, 106, 100), new Pixel(212, 136, 32),
                new Pixel(160, 170, 0), new Pixel(116, 196, 0), new Pixel(76, 208, 32), new Pixel(56, 204, 108),
                new Pixel(56, 180, 204), new Pixel(60, 60, 60), new Pixel(0, 0, 0), new Pixel(0, 0, 0),
                new Pixel(236, 238, 236), new Pixel(168, 204, 236), new Pixel(188, 188, 236), new Pixel(212, 178, 236),
                new Pixel(236, 174, 236), new Pixel(236, 174, 212), new Pixel(236, 180, 176), new Pixel(228, 196, 144),
                new Pixel(204, 210, 120), new Pixel(180, 222, 120), new Pixel(168, 226, 144), new Pixel(152, 226, 180),
                new Pixel(160, 214, 228), new Pixel(160, 162, 160), new Pixel(0, 0, 0), new Pixel(0, 0, 0),
            };
        }

        public void ConnectCartridge(Cartridge cartridge)
        {
            _cartridge = cartridge;
        }

        public void Reset()
        {
            for (var x = 0; x < Screen.Width; x++)
            for (var y = 0; y < Screen.Height; y++)
                Screen[x, y] = Pixel.Presets.White;
        }

        public void Clock()
        {
            if (ScanLine >= 241 && ScanLine < 261)
            {
                if (ScanLine == 241 && Cycle == 1)
                    _statusRegister |= (byte) StatusRegister.VerticalBlank;
            }

            Screen[Cycle - 1, ScanLine] = _palScreen[Cycle * ScanLine % 32 > 16 ? 0x3F : 0x30];
            Cycle++;

            if (Cycle >= 341)
            {
                Cycle = 0;
                ScanLine++;

                if (ScanLine >= 261)
                {
                    ScanLine = -1;
                    FrameComplete = true;
                }
            }
        }

        public byte CpuRead(ushort address)
        {
            var data = (byte) 0;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (address)
            {
                case 0x0000:
                    return 0;

                case 0x0001:
                    return 0;

                case 0x0002:
                    data = (byte) ((_statusRegister & 0xE0) | (_ppuDataBuffer & 0x1F));

                    _statusRegister &= (byte) ~StatusRegister.VerticalBlank;
                    _addressLatch = 0;

                    return data;

                case 0x0003:
                    return 0;

                case 0x0004:
                    return 0;

                case 0x0005:
                    return 0;

                case 0x0006:
                    return 0;

                case 0x0007:
                    data = _ppuDataBuffer;

                    _ppuDataBuffer = PpuRead(_vramAddress);
                    
                    if (_vramAddress >= 0x3F00)
                        data = _ppuDataBuffer;

                    _vramAddress += (ushort) ((_controlRegister & (int) ControlRegister.IncrementMode) > 0 ? 32 : 1);

                    return data;

            }

            throw new NotImplementedException();
        }

        public byte PpuRead(ushort address)
        {
            address &= 0x3FFF;

            if (_cartridge.PpuRead(address, out var value))
                return value;

            if (address <= 0x1FFF)
                return _patternTables[(address & 0x1000) >> 12, address & 0x0FFF];

            if (address >= 0x3F00 && address <= 0x3FFF)
            {
                address &= 0x001F;

                if (address == 0x0010)
                    address = 0x0000;

                if (address == 0x0014)
                    address = 0x0004;

                if (address == 0x0018)
                    address = 0x0008;

                if (address == 0x001C)
                    address = 0x000C;

                return (byte) (_palette[address] & ((_maskRegister & (int) MaskRegister.GrayScale) > 0 ? 0x30 : 0x3F));
            }

            throw new NotImplementedException();
        }

        public void CpuWrite(ushort address, byte value)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (address)
            {
                case 0x0000:
                    _controlRegister = value;

                    _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.NametableX) | _controlRegister & (int)ControlRegister.NametableX);
                    _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.NametableY) | _controlRegister & (int)ControlRegister.NametableY);

                    return;

                case 0x0001:
                    _maskRegister = value;

                    return;

                case 0x0002:
                    return;

                case 0x0003:
                    return;

                case 0x0004:
                    return;

                case 0x0005:
                    if (_addressLatch == 0)
                    {
                        var valueShifted = value >> 3;

                        _addressLatch = 1;
                        _fineX = (byte) (value & 0x07);

                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseX1) | valueShifted & 1 << 0);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseX2) | valueShifted & 1 << 1);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseX3) | valueShifted & 1 << 2);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseX4) | valueShifted & 1 << 3);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseX5) | valueShifted & 1 << 4);
                    }
                    else
                    {
                        var valueShifted = value >> 3;
                        var valueOrd = value & 0x07;

                        _addressLatch = 0;

                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.FineY1) | valueOrd & 1 << 0);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.FineY2) | valueOrd & 1 << 1);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.FineY3) | valueOrd & 1 << 2);

                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseY1) | valueShifted & 1 << 0);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseY2) | valueShifted & 1 << 1);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseY3) | valueShifted & 1 << 2);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseY4) | valueShifted & 1 << 3);
                        _tramAddress = (ushort)(_tramAddress & (int)~(LoopyRegister.CoarseY5) | valueShifted & 1 << 4);
                    }

                    return;

                case 0x0006:
                    if (_addressLatch == 0)
                    {
                        _tramAddress = (ushort) (((value & 0x3F) << 8) | (_tramAddress & 0x00FF));
                        _addressLatch = 1;
                    }
                    else
                    {
                        _tramAddress = (ushort) ((_tramAddress & 0xFF00) | value);
                        _vramAddress = _tramAddress;
                        _addressLatch = 0;
                    }

                    return;

                case 0x0007:
                    PpuWrite(_vramAddress, value);

                    _vramAddress += (ushort)((_controlRegister & (int)ControlRegister.IncrementMode) > 0 ? 32 : 1);

                    return;

            }

            throw new NotImplementedException();
        }

        public void PpuWrite(ushort address, byte value)
        {
            address &= 0x3FFF;

            if (_cartridge.PpuWrite(address, value))
                return;

            if (address <= 0x1FFF)
            {
                _patternTables[(address & 0x1000) >> 12, address & 0x0FFF] = value;
                
                return;
            }

            if (address >= 0x2000 && address <= 0x3EFF)
            {
                address &= 0x0FFF;

                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (_cartridge.MirroringMode)
                {
                    case MirroringMode.Vertical when address <= 0x03FF:
                        _nameTables[0, address & 0x03FF] = value;
                        break;

                    case MirroringMode.Vertical when address >= 0x0400 && address <= 0x07FF:
                        _nameTables[1, address & 0x03FF] = value;
                        break;

                    case MirroringMode.Vertical when address >= 0x0800 && address <= 0x0BFF:
                        _nameTables[0, address & 0x03FF] = value;
                        break;

                    case MirroringMode.Vertical:
                        if (address >= 0x0C00 && address <= 0x0FFF)
                            _nameTables[1, address & 0x03FF] = value;

                        break;

                    case MirroringMode.Horizontal when address <= 0x03FF:
                    case MirroringMode.Horizontal when address >= 0x0400 && address <= 0x07FF:
                        _nameTables[0, address & 0x03FF] = value;
                        break;

                    case MirroringMode.Horizontal when address >= 0x0800 && address <= 0x0BFF:
                        _nameTables[1, address & 0x03FF] = value;
                        break;

                    case MirroringMode.Horizontal:
                        if (address >= 0x0C00 && address <= 0x0FFF)
                            _nameTables[1, address & 0x03FF] = value;

                        break;
                }

                return;
            }

            if (address >= 0x3F00 && address <= 0x3FFF)
            {
                address &= 0x001F;

                if (address == 0x0010)
                    address = 0x0000;

                if (address == 0x0014)
                    address = 0x0004;

                if (address == 0x0018)
                    address = 0x0008;

                if (address == 0x001C)
                    address = 0x000C;

                _palette[address] = value;

                return;
            }

            throw new NotImplementedException();
        }
    }
}
