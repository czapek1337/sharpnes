﻿using System;

using nes_emulator.CPU;

namespace nes_emulator
{
    internal class DataBus
    {
        private int _systemClockCounter;
        private readonly byte[] _cpuRam = new byte[0x800];
        
        private readonly byte[] _controllerState = new byte[2];

        private Cartridge _cartridge;

        public AudioProcessor Apu { get; }
        public PictureProcessor Ppu { get; }
        public Processor Cpu { get; }

        public byte[] controller = new byte[2];

        public DataBus()
        {
            Apu = new AudioProcessor();
            Ppu = new PictureProcessor();
            Cpu = new Processor();
        }

        public void ConnectCartridge(Cartridge cartridge)
        {
            _cartridge = cartridge;

            Ppu.ConnectCartridge(cartridge);
            Cpu.ConnectBus(this);

            Reset();
        }

        public void Reset()
        {
            _cartridge.Reset();
            Ppu.Reset();
            Cpu.Reset();

            _systemClockCounter = 0;
        }

        public void Clock()
        {
            Ppu.Clock();

            if (_systemClockCounter % 3 == 0)
            {
#if DEBUG
                Cpu.Trace();
#endif

                Cpu.Clock();
            }

            _systemClockCounter++;
        }

        public byte CpuRead(ushort address)
        {
            if (_cartridge.CpuRead(address, out var value))
                return value;

            if (address <= 0x1FFF)
                return _cpuRam[address & 0x7FF];

            if (address >= 0x2000 && address <= 0x3FFF)
                return Ppu.CpuRead((ushort) (address & 0x0007));

            if (address == 0x4016 || address == 0x4017)
            {
                var data = (byte)((_controllerState[address & 0x0001] & 0x80) > 0 ? 1 : 0);

                _controllerState[address & 0x0001] <<= 1;

                return data;
            }

            return 0;
        }

        public void CpuWrite(ushort address, byte value)
        {
            if (_cartridge.CpuWrite(address, value))
                return;

            if (address <= 0x1FFF)
                _cpuRam[address & 0x07FF] = value;
            else if (address >= 0x2000 && address <= 0x3FFF)
                Ppu.CpuWrite((ushort) (address & 0x0007), value);
            else if (address >= 0x4016 && address <= 0x4017)
                _controllerState[address & 0x0001] = controller[address & 0x0001];
        }
    }
}
