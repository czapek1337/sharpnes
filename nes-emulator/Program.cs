﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using PixelEngine;

namespace nes_emulator
{
    internal class NesEmulator : Game
    {
        private static Cartridge _cartridge;
        private static DataBus _bus;

        public NesEmulator(string romPath)
        {
            _cartridge = new Cartridge(romPath);
            _bus = new DataBus();

            _bus.ConnectCartridge(_cartridge);
            _bus.Reset();
        }

        public static void Main(string[] args)
        {
            const string defaultRom = "../nestest.nes";

            if (args.Length == 0 && !File.Exists(defaultRom))
            {
                Console.WriteLine($"Usage: {Process.GetCurrentProcess().ProcessName} <.nes file path>");
                Environment.Exit(0);
            }

            var emulator = new NesEmulator(args.Length == 0 ? defaultRom : args[0]);

            emulator.Construct(1024, 768, 1, 1);
            emulator.Start();
        }

        private void DrawRegister(Point position, int width, string name, string valueStr, long lastUpdate)
        {
            var timeSinceLastUpdate = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() - lastUpdate;
            var redChannelAmount = 255 - timeSinceLastUpdate;

            if (redChannelAmount < 16)
                redChannelAmount = 16;

            if (redChannelAmount > 255)
                redChannelAmount = 255;

            DrawRect(new Point(position.X - 1, position.Y - 1), width + 1, 31, Pixel.Presets.White);
            FillRect(position, width, 30, new Pixel((byte) redChannelAmount, 16, 16));

            DrawText(new Point(position.X + (int)(width * 0.5f) - (int) (8 * name.Length * 0.5), position.Y + 6), name, Pixel.Presets.White);
            DrawText(new Point(position.X + (int)(width * 0.5f) - (int) (8 * valueStr.Length * 0.5), position.Y + 18), valueStr, Pixel.Presets.White);
        }

        private void DrawSpriteOutlined(Point position, Sprite sprite, Pixel outlineColor)
        {
            DrawRect(new Point(position.X - 1, position.Y - 1), sprite.Width + 1, sprite.Height + 1, outlineColor);
            DrawSprite(position, sprite);
        }

        private void DrawPalette(Point position, int size, int index)
        {
            DrawRect(new Point(position.X - 1, position.Y - 1), size * 4 + 1, size + 1, Pixel.Presets.White);

            for (var i = 0; i < 4; i++)
            {
                FillRect(new Point(position.X + i * size, position.Y), size, size, _bus.Ppu.GetColorFromPaletteRam(index, i));
            }
        }

        public override void OnUpdate(float elapsed)
        {
            _bus.controller[0] = 0x00;
            _bus.controller[0] |= (byte) (GetKey(Key.X).Down ? 0x80 : 0x00);
            _bus.controller[0] |= (byte) (GetKey(Key.Z).Down ? 0x40 : 0x00);
            _bus.controller[0] |= (byte) (GetKey(Key.A).Down ? 0x20 : 0x00);
            _bus.controller[0] |= (byte) (GetKey(Key.S).Down ? 0x10 : 0x00);
            _bus.controller[0] |= (byte) (GetKey(Key.Up).Down ? 0x08 : 0x00);
            _bus.controller[0] |= (byte) (GetKey(Key.Down).Down ? 0x04 : 0x00);
            _bus.controller[0] |= (byte) (GetKey(Key.Left).Down ? 0x02 : 0x00);
            _bus.controller[0] |= (byte) (GetKey(Key.Right).Down ? 0x01 : 0x00);

            Clear(Pixel.Presets.Black);

            do
            {
                _bus.Clock();

            } while (!_bus.Ppu.FrameComplete);

            _bus.Ppu.FrameComplete = false;

            DrawSpriteOutlined(new Point(8, 8), _bus.Ppu.Screen, Pixel.Presets.White);
            DrawSpriteOutlined(new Point(8, 256), _bus.Ppu.NameTables[0], Pixel.Presets.White);
            DrawSpriteOutlined(new Point(272, 256), _bus.Ppu.NameTables[1], Pixel.Presets.White);
            DrawSpriteOutlined(new Point(8, 504), _bus.Ppu.GetPatternTable(0), Pixel.Presets.White);
            DrawSpriteOutlined(new Point(144, 504), _bus.Ppu.GetPatternTable(1), Pixel.Presets.White);

            for (int i = 0, swatchSize = 12; i < 4; i++)
            {
                DrawPalette(new Point(280 + 8 * i + swatchSize * 4 * i, 504), swatchSize, i);
                DrawPalette(new Point(280 + 8 * i + swatchSize * 4 * i, 504 + swatchSize + 8), swatchSize, i + 4);
            }

            DrawRegister(new Point(272, 8), 36, "A", $"${_bus.Cpu.Accumulator:X02}", _bus.Cpu.LastAccumulatorUpdate);
            DrawRegister(new Point(316, 8), 36, "X", $"${_bus.Cpu.RegisterX:X02}", _bus.Cpu.LastRegisterXUpdate);
            DrawRegister(new Point(360, 8), 36, "Y", $"${_bus.Cpu.RegisterY:X02}", _bus.Cpu.LastRegisterYUpdate);
            DrawRegister(new Point(404, 8), 36, "SP", $"${_bus.Cpu.StackPointer:X02}", _bus.Cpu.LastStackPointerUpdate);
            DrawRegister(new Point(448, 8), 36, "F", $"${_bus.Cpu.Flags.GetByte():X02}", _bus.Cpu.Flags.LastUpdate);
            DrawRegister(new Point(494, 8), 52, "PC", $"${_bus.Cpu.ProgramCounter:X04}", _bus.Cpu.LastProgramCounterUpdate);
        }
    }
}
