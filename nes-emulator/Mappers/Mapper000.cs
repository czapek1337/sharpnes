﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nes_emulator.Mappers
{
    internal class Mapper000 : IMapper
    {
        private readonly int _programBanks;
        private readonly int _characterBanks;

        public Mapper000(int programBanks, int characterBanks)
        {
            _programBanks = programBanks;
            _characterBanks = characterBanks;

            Reset();
        }

        public bool CpuMapRead(ushort address, out uint mappedAddress)
        {
            if (address >= 0x8000 && address <= 0xFFFF)
            {
                mappedAddress = (uint) (address & (_programBanks > 1 ? 0x7FFF : 0x3FFF));

                return true;
            }

            mappedAddress = 0;

            return false;
        }

        public bool CpuMapWrite(ushort address, out uint mappedAddress, byte data = 0)
        {
            if (address >= 0x8000 && address <= 0xFFFF)
            {
                mappedAddress = (uint) (address & (_programBanks > 1 ? 0x7FFF : 0x3FFF));

                return true;
            }

            mappedAddress = 0;

            return false;
        }

        public bool PpuMapRead(ushort address, out uint mappedAddress)
        {
            if (address <= 0x1FFF)
            {
                mappedAddress = address;

                return true;
            }

            mappedAddress = 0;

            return false;
        }

        public bool PpuMapWrite(ushort address, out uint mappedAddress, byte data = 0)
        {
            if (address <= 0x1FFF && _characterBanks == 0)
            {
                mappedAddress = address;

                return true;
            }

            mappedAddress = 0;

            return false;
        }

        public void Reset()
        {
            
        }
    }
}
