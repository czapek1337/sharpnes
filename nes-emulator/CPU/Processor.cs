﻿using System;

namespace nes_emulator.CPU
{
    internal enum InterruptType
    {
        NonMaskable,
        Maskable,
        Break,
        Reset,
    }

    internal enum RegisterType
    {
        Accumulator,
        RegisterX,
        RegisterY,
        Flags,
        StackPointer
    }

    internal partial class Processor
    {
        private byte _accumulator;
        private byte _registerX;
        private byte _registerY;
        private byte _stackPointer;
        private ushort _programCounter;
        
        public long LastAccumulatorUpdate { get; private set; }
        public long LastRegisterXUpdate { get; private set; }
        public long LastRegisterYUpdate { get; private set; }
        public long LastStackPointerUpdate { get; private set; }
        public long LastProgramCounterUpdate { get; private set; }

        public byte Accumulator { get => _accumulator; private set {_accumulator = value; LastAccumulatorUpdate = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(); } }
        public byte RegisterX { get => _registerX; private set { _registerX = value; LastRegisterXUpdate = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(); } }
        public byte RegisterY { get => _registerY; private set { _registerY = value; LastRegisterYUpdate = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(); } }
        public byte StackPointer { get => _stackPointer; private set { _stackPointer = value; LastStackPointerUpdate = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(); } }
        public ushort ProgramCounter { get => _programCounter; private set { _programCounter = value; LastProgramCounterUpdate = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(); } }
        public ProcessorFlags Flags { get; }

        private DataBus _bus;

        private byte GetRegisterValue(RegisterType register)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (register)
            {
                case RegisterType.Accumulator:
                    return Accumulator;

                case RegisterType.RegisterX:
                    return RegisterX;

                case RegisterType.RegisterY:
                    return RegisterY;

                case RegisterType.StackPointer:
                    return StackPointer;

            }

            throw new NotImplementedException();
        }

        private byte SetRegisterValue(RegisterType register, byte value)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (register)
            {
                case RegisterType.Accumulator:
                    return Accumulator = value;

                case RegisterType.RegisterX:
                    return RegisterX = value;

                case RegisterType.RegisterY:
                    return RegisterY = value;

                case RegisterType.StackPointer:
                    return StackPointer = value;

            }

            throw new NotImplementedException();
        }

        private void Push(byte value)
        {
            Write((ushort) (0x0100 + (StackPointer--)), value);
        }

        private byte Pop()
        {
            return Read((ushort) (0x0100 + (++StackPointer)));
        }

        public Processor()
        {
            Flags = new ProcessorFlags();
        }

        public void ConnectBus(DataBus bus)
        {
            _bus = bus;
        }

        public void Reset()
        {
            Accumulator = 0;
            RegisterX = 0;
            RegisterY = 0;
            StackPointer = 0xFD;
            ProgramCounter = (ushort) (Read(0xFFFC) | Read(0xFFFD) << 8);
            Flags.SetByte(0x24);
        }

        public void Trace()
        {
            var opcode = Read(ProgramCounter);
            var operandByte1 = Read((ushort) (ProgramCounter + 1));
            var operandByte2 = Read((ushort) (ProgramCounter + 2));

            var addressingMode = GetAddressingMode(opcode);

            var tempLine1 = "";
            var tempLine2 = "";

            var traceLine = $"${ProgramCounter:X04}   ";
            var mnemonic = GetMnemonic(opcode);

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (GetBytesNeeded(addressingMode))
            {
                case 0:
                    tempLine1 = $"{opcode:X02}";
                    tempLine2 = mnemonic;

                    break;

                case 1:
                    tempLine1 = $"{opcode:X02} {operandByte1:X02}";
                    tempLine2 = $"{mnemonic} ${operandByte1:X02}";

                    break;

                case 2:
                    tempLine1 = $"{opcode:X02} {operandByte1:X02} {operandByte2:X02}";
                    tempLine2 = $"{mnemonic} ${operandByte2:X02}{operandByte1:X02}";

                    break;

            }

            var flagsString = $"{(Flags.GetFlag(ProcessorFlag.Negative) ? 'N' : '-')}" +
                              $"{(Flags.GetFlag(ProcessorFlag.Overflow) ? 'V' : '-')}" +
                              $"{(Flags.GetFlag(ProcessorFlag.Unused) ? 'U' : '-')}" +
                              $"{(Flags.GetFlag(ProcessorFlag.Break) ? 'B' : '-')}" +
                              $"{(Flags.GetFlag(ProcessorFlag.Decimal) ? 'D' : '-')}" +
                              $"{(Flags.GetFlag(ProcessorFlag.InterruptDisable) ? 'I' : '-')}" +
                              $"{(Flags.GetFlag(ProcessorFlag.Zero) ? 'Z' : '-')}" +
                              $"{(Flags.GetFlag(ProcessorFlag.Carry) ? 'C' : '-')}";

            traceLine += $"{tempLine1.PadRight(11)} {tempLine2.PadRight(11)}";

            Console.WriteLine(
                $"{traceLine} A: ${Accumulator:X02}  X: ${RegisterX:X02}  Y: ${RegisterY:X02}  S: ${StackPointer:X02}  F: {flagsString} (${Flags.GetByte():X02})");
        }

        public void Clock()
        {
            var opcode = Read(ProgramCounter++);

            var addressingMode = GetAddressingMode(opcode);
            var instructionHandler = GetHandler(opcode);
            
            instructionHandler(addressingMode);
        }

        public void Interrupt(InterruptType type)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (type)
            {
                case InterruptType.Maskable when !Flags.GetFlag(ProcessorFlag.InterruptDisable):
                    Push((byte) (ProgramCounter >> 8));
                    Push((byte) (ProgramCounter & 0xFF));

                    Flags.SetFlag(ProcessorFlag.Break, false);
                    Flags.SetFlag(ProcessorFlag.Unused, true);
                    Flags.SetFlag(ProcessorFlag.InterruptDisable, true);

                    Push(Flags.GetByte());

                    ProgramCounter = (ushort)(Read(0xFFFE) | Read(0xFFFF) << 8);

                    break;

                case InterruptType.NonMaskable:
                    Push((byte)(ProgramCounter >> 8));
                    Push((byte)(ProgramCounter & 0xFF));

                    Flags.SetFlag(ProcessorFlag.Break, false);
                    Flags.SetFlag(ProcessorFlag.Unused, true);
                    Flags.SetFlag(ProcessorFlag.InterruptDisable, true);

                    Push(Flags.GetByte());

                    ProgramCounter = (ushort)(Read(0xFFFA) | Read(0xFFFB) << 8);

                    break;

                case InterruptType.Reset:
                    Reset();

                    break;

                case InterruptType.Break:
                    Push((byte)(ProgramCounter >> 8));
                    Push((byte)(ProgramCounter & 0xFF));

                    Flags.SetFlag(ProcessorFlag.InterruptDisable, true);
                    Flags.SetFlag(ProcessorFlag.Break, true);

                    Push(Flags.GetByte());

                    Flags.SetFlag(ProcessorFlag.Break, false);
                    ProgramCounter = (ushort)(Read(0xFFFE) | Read(0xFFFF) << 8);

                    break;

            }
        }

        public byte Read(ushort address) { return _bus.CpuRead(address); }
        public void Write(ushort address, byte value) { _bus.CpuWrite(address, value); }
    }
}
