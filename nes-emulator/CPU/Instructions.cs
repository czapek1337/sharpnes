﻿using System;

namespace nes_emulator.CPU
{
    // ReSharper disable MemberCanBeMadeStatic.Local
    internal partial class Processor
    {
        private void UpdateFlagsNz(byte value)
        {
            Flags.SetFlag(ProcessorFlag.Zero, value == 0);
            Flags.SetFlag(ProcessorFlag.Negative, (value & 0x80) > 0);
        }

        private void GenericHandler(AddressingMode addressingMode)
        {
            if (addressingMode == AddressingMode.Implied)
                return;

            ProgramCounter += (ushort) GetBytesNeeded(addressingMode);
        }

        private void JumpHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);

            if (addressingMode == AddressingMode.Indirect)
                resolvedAddress = (ushort) (Read(resolvedAddress) | (ushort) (Read((ushort) (resolvedAddress + 1)) << 8));

            ProgramCounter = resolvedAddress;
        }

        private void JumpToSubroutineHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);

            if (addressingMode == AddressingMode.Indirect)
                resolvedAddress = (ushort) (Read(resolvedAddress) | (ushort) (Read((ushort) (resolvedAddress + 1)) << 8));

            ProgramCounter += (ushort) GetBytesNeeded(addressingMode);

            Push((byte) (ProgramCounter >> 8));
            Push((byte) (ProgramCounter & 0xFF));

            ProgramCounter = resolvedAddress;
        }

        private void BitTestHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = (ushort) Read(resolvedAddress);
            var tempValue = Accumulator & fetchedValue;

            Flags.SetFlag(ProcessorFlag.Zero, (tempValue & 0x00FF) == 0x00);
            Flags.SetFlag(ProcessorFlag.Negative, (fetchedValue & 1 << 7) > 0);
            Flags.SetFlag(ProcessorFlag.Overflow, (fetchedValue & 1 << 6) > 0);

            GenericHandler(addressingMode);
        }

        private void ReturnHandler(AddressingMode addressingMode, bool pullFlags)
        {
            if (pullFlags)
                Flags.SetByte(Pop());

            ProgramCounter = (ushort) (Pop() | (ushort) (Pop() << 8));
        }

        private void LoadHandler(AddressingMode addressingMode, RegisterType targetRegister)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);

            UpdateFlagsNz(SetRegisterValue(targetRegister, fetchedValue));
            GenericHandler(addressingMode);
        }

        private void StoreHandler(AddressingMode addressingMode, byte source)
        {
            var resolvedAddress = ResolveAddress(addressingMode);

            Write(resolvedAddress, source);
            GenericHandler(addressingMode);
        }

        private void TransferHandler(AddressingMode addressingMode, RegisterType source, RegisterType targetRegister, bool updateFlags)
        {
            SetRegisterValue(targetRegister, GetRegisterValue(source));

            if (updateFlags)
                UpdateFlagsNz(GetRegisterValue(targetRegister));

            GenericHandler(addressingMode);
        }

        private void BranchHandler(AddressingMode addressingMode, ProcessorFlag flag, bool state)
        {
            if (Flags.GetFlag(flag) == state)
                ProgramCounter += ResolveAddress(addressingMode);
            else
                GenericHandler(addressingMode);
        }

        private void CompareHandler(AddressingMode addressingMode, byte register)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = (ushort) Read(resolvedAddress);
            var tempValue = register - fetchedValue;

            Flags.SetFlag(ProcessorFlag.Carry, register >= fetchedValue);
            Flags.SetFlag(ProcessorFlag.Zero, (tempValue & 0x00FF) == 0);
            Flags.SetFlag(ProcessorFlag.Negative, (tempValue & 0x0080) != 0);

            GenericHandler(addressingMode);
        }

        private void AddWithCarryHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);
            var tempValue = Accumulator + fetchedValue + (Flags.GetFlag(ProcessorFlag.Carry) ? 1 : 0);

            Flags.SetFlag(ProcessorFlag.Carry, tempValue > 255);
            Flags.SetFlag(ProcessorFlag.Zero, (tempValue & 0x00FF) == 0);
            Flags.SetFlag(ProcessorFlag.Negative, (tempValue & 0x80) > 0);
            Flags.SetFlag(ProcessorFlag.Overflow, (~(Accumulator ^ fetchedValue) & (Accumulator ^ (ushort) tempValue) & 0x0080) > 0);

            Accumulator = (byte) (tempValue & 0x00FF);

            GenericHandler(addressingMode);
        }

        private void BitwiseAndHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);

            Accumulator = (byte) (Accumulator & fetchedValue);

            UpdateFlagsNz(Accumulator);
            GenericHandler(addressingMode);
        }

        private void BitwiseOrHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);

            Accumulator = (byte)(Accumulator | fetchedValue);

            UpdateFlagsNz(Accumulator);
            GenericHandler(addressingMode);
        }

        private void BitwiseXorHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);

            Accumulator = (byte)(Accumulator ^ fetchedValue);

            UpdateFlagsNz(Accumulator);
            GenericHandler(addressingMode);
        }

        private void ArithmeticShiftLeftHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);
            var tempValue = fetchedValue << 1;

            Flags.SetFlag(ProcessorFlag.Carry, (tempValue & 0xFF00) > 0);
            Flags.SetFlag(ProcessorFlag.Zero, (tempValue & 0x00FF) == 0x00);
            Flags.SetFlag(ProcessorFlag.Negative, (tempValue & 0x0080) > 0);

            if (addressingMode == AddressingMode.Implied)
                Accumulator = (byte)(tempValue & 0x00FF);
            else
                Write(resolvedAddress, (byte)(tempValue & 0x00FF));

            GenericHandler(addressingMode);
        }

        private void LogicalShiftRightHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);
            var tempValue = fetchedValue >> 1;

            Flags.SetFlag(ProcessorFlag.Carry, (fetchedValue & 0x0001) > 0);
            Flags.SetFlag(ProcessorFlag.Zero, (tempValue & 0x00FF) == 0x00);
            Flags.SetFlag(ProcessorFlag.Negative, (tempValue & 0x0080) > 0);

            if (addressingMode == AddressingMode.Implied)
                Accumulator = (byte)(tempValue & 0x00FF);
            else
                Write(resolvedAddress, (byte)(tempValue & 0x00FF));

            GenericHandler(addressingMode);
        }

        private void RotateLeftHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);
            var tempValue = (ushort)((Flags.GetFlag(ProcessorFlag.Carry) ? 1 : 0) << 7) | (fetchedValue >> 1);

            Flags.SetFlag(ProcessorFlag.Carry, (fetchedValue & 0x01) > 0);
            Flags.SetFlag(ProcessorFlag.Zero, (tempValue & 0x00FF) == 0x00);
            Flags.SetFlag(ProcessorFlag.Negative, (tempValue & 0x0080) > 0);

            if (addressingMode == AddressingMode.Implied)
                Accumulator = (byte)(tempValue & 0x00FF);
            else
                Write(resolvedAddress, (byte)(tempValue & 0x00FF));

            GenericHandler(addressingMode);
        }

        private void RotateRightHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress);
            var tempValue = (ushort) (fetchedValue << 1) | (Flags.GetFlag(ProcessorFlag.Carry) ? 1 : 0);

            Flags.SetFlag(ProcessorFlag.Carry, (tempValue & 0xFF00) > 0);
            Flags.SetFlag(ProcessorFlag.Zero, (tempValue & 0x00FF) == 0x00);
            Flags.SetFlag(ProcessorFlag.Negative, (tempValue & 0x0080) > 0);

            if (addressingMode == AddressingMode.Implied)
                Accumulator = (byte) (tempValue & 0x00FF);
            else
                Write(resolvedAddress, (byte) (tempValue & 0x00FF));
            
            GenericHandler(addressingMode);
        }

        private void DecrementMemoryHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress) - 1;

            Write(resolvedAddress, (byte)(fetchedValue & 0x00FF));
            UpdateFlagsNz((byte)(fetchedValue & 0x00FF));

            GenericHandler(addressingMode);
        }

        private void DecrementRegisterHandler(AddressingMode addressingMode, RegisterType register)
        {
            SetRegisterValue(register, (byte) (GetRegisterValue(register) - 1));
            UpdateFlagsNz(GetRegisterValue(register));
            GenericHandler(addressingMode);
        }

        private void IncrementMemoryHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress) + 1;

            Write(resolvedAddress, (byte) (fetchedValue & 0x00FF));
            UpdateFlagsNz((byte) (fetchedValue & 0x00FF));

            GenericHandler(addressingMode);
        }

        private void IncrementRegisterHandler(AddressingMode addressingMode, RegisterType register)
        {
            SetRegisterValue(register, (byte)(GetRegisterValue(register) + 1));
            UpdateFlagsNz(GetRegisterValue(register));
            GenericHandler(addressingMode);
        }

        private void SubtractWithCarryHandler(AddressingMode addressingMode)
        {
            var resolvedAddress = ResolveAddress(addressingMode);
            var fetchedValue = Read(resolvedAddress) ^ 0x00FF;
            var tempValue = Accumulator + fetchedValue + (Flags.GetFlag(ProcessorFlag.Carry) ? 1 : 0);

            Flags.SetFlag(ProcessorFlag.Carry, (tempValue & 0xFF00) > 0);
            Flags.SetFlag(ProcessorFlag.Zero, (tempValue & 0x00FF) == 0);
            Flags.SetFlag(ProcessorFlag.Overflow, ((tempValue ^ Accumulator) & (tempValue ^ fetchedValue) & 0x0080) > 0);
            Flags.SetFlag(ProcessorFlag.Negative, (tempValue & 0x0080) > 0);

            Accumulator = (byte) (tempValue & 0x00FF);

            GenericHandler(addressingMode);
        }

        private void Jmp(AddressingMode addressingMode) => JumpHandler(addressingMode);
        private void Jsr(AddressingMode addressingMode) => JumpToSubroutineHandler(addressingMode);
        private void Bit(AddressingMode addressingMode) => BitTestHandler(addressingMode);
        private void Plp(AddressingMode addressingMode) => Flags.SetByte(Pop());
        private void Pla(AddressingMode addressingMode) => UpdateFlagsNz(Accumulator = Pop());
        private void Php(AddressingMode addressingMode) => Push((byte) (Flags.GetByte() | 1 << 4));
        private void Pha(AddressingMode addressingMode) => Push(Accumulator);
        private void Rti(AddressingMode addressingMode) => ReturnHandler(addressingMode, true);
        private void Rts(AddressingMode addressingMode) => ReturnHandler(addressingMode, false);
        private void Lda(AddressingMode addressingMode) => LoadHandler(addressingMode, RegisterType.Accumulator);
        private void Ldx(AddressingMode addressingMode) => LoadHandler(addressingMode, RegisterType.RegisterX);
        private void Ldy(AddressingMode addressingMode) => LoadHandler(addressingMode, RegisterType.RegisterY);
        private void Sta(AddressingMode addressingMode) => StoreHandler(addressingMode, Accumulator);
        private void Stx(AddressingMode addressingMode) => StoreHandler(addressingMode, RegisterX);
        private void Sty(AddressingMode addressingMode) => StoreHandler(addressingMode, RegisterY);
        private void Tax(AddressingMode addressingMode) => TransferHandler(addressingMode, RegisterType.Accumulator, RegisterType.RegisterX, true);
        private void Tay(AddressingMode addressingMode) => TransferHandler(addressingMode, RegisterType.Accumulator, RegisterType.RegisterY, true);
        private void Tsx(AddressingMode addressingMode) => TransferHandler(addressingMode, RegisterType.StackPointer, RegisterType.RegisterX, true);
        private void Txa(AddressingMode addressingMode) => TransferHandler(addressingMode, RegisterType.RegisterX, RegisterType.Accumulator, true);
        private void Txs(AddressingMode addressingMode) => TransferHandler(addressingMode, RegisterType.RegisterX, RegisterType.StackPointer, false);
        private void Tya(AddressingMode addressingMode) => TransferHandler(addressingMode, RegisterType.RegisterY, RegisterType.Accumulator, true);
        private void Bcc(AddressingMode addressingMode) => BranchHandler(addressingMode, ProcessorFlag.Carry, false);
        private void Bcs(AddressingMode addressingMode) => BranchHandler(addressingMode, ProcessorFlag.Carry, true);
        private void Beq(AddressingMode addressingMode) => BranchHandler(addressingMode, ProcessorFlag.Zero, true);
        private void Bmi(AddressingMode addressingMode) => BranchHandler(addressingMode, ProcessorFlag.Negative, true);
        private void Bne(AddressingMode addressingMode) => BranchHandler(addressingMode, ProcessorFlag.Zero, false);
        private void Bpl(AddressingMode addressingMode) => BranchHandler(addressingMode, ProcessorFlag.Negative, false);
        private void Bvc(AddressingMode addressingMode) => BranchHandler(addressingMode, ProcessorFlag.Overflow, false);
        private void Bvs(AddressingMode addressingMode) => BranchHandler(addressingMode, ProcessorFlag.Overflow, true);
        private void Clc(AddressingMode addressingMode) => Flags.SetFlag(ProcessorFlag.Carry, false);
        private void Cld(AddressingMode addressingMode) => Flags.SetFlag(ProcessorFlag.Decimal, false);
        private void Cli(AddressingMode addressingMode) => Flags.SetFlag(ProcessorFlag.InterruptDisable, false);
        private void Clv(AddressingMode addressingMode) => Flags.SetFlag(ProcessorFlag.Overflow, false);
        private void Sec(AddressingMode addressingMode) => Flags.SetFlag(ProcessorFlag.Carry, true);
        private void Sed(AddressingMode addressingMode) => Flags.SetFlag(ProcessorFlag.Decimal, true);
        private void Sei(AddressingMode addressingMode) => Flags.SetFlag(ProcessorFlag.InterruptDisable, true);
        private void Cmp(AddressingMode addressingMode) => CompareHandler(addressingMode, Accumulator);
        private void Cpx(AddressingMode addressingMode) => CompareHandler(addressingMode, RegisterX);
        private void Cpy(AddressingMode addressingMode) => CompareHandler(addressingMode, RegisterY);
        private void Adc(AddressingMode addressingMode) => AddWithCarryHandler(addressingMode);
        private void And(AddressingMode addressingMode) => BitwiseAndHandler(addressingMode);
        private void Asl(AddressingMode addressingMode) => ArithmeticShiftLeftHandler(addressingMode);
        private void Brk(AddressingMode addressingMode) => Interrupt(InterruptType.Break);
        private void Dec(AddressingMode addressingMode) => DecrementMemoryHandler(addressingMode);
        private void Dex(AddressingMode addressingMode) => DecrementRegisterHandler(addressingMode, RegisterType.RegisterX);
        private void Dey(AddressingMode addressingMode) => DecrementRegisterHandler(addressingMode, RegisterType.RegisterY);
        private void Eor(AddressingMode addressingMode) => BitwiseXorHandler(addressingMode);
        private void Inc(AddressingMode addressingMode) => IncrementMemoryHandler(addressingMode);
        private void Inx(AddressingMode addressingMode) => IncrementRegisterHandler(addressingMode, RegisterType.RegisterX);
        private void Iny(AddressingMode addressingMode) => IncrementRegisterHandler(addressingMode, RegisterType.RegisterY);
        private void Lsr(AddressingMode addressingMode) => LogicalShiftRightHandler(addressingMode);
        private void Nop(AddressingMode addressingMode) => GenericHandler(addressingMode);
        private void Ora(AddressingMode addressingMode) => BitwiseOrHandler(addressingMode);
        private void Rol(AddressingMode addressingMode) => RotateLeftHandler(addressingMode);
        private void Ror(AddressingMode addressingMode) => RotateRightHandler(addressingMode);
        private void Sbc(AddressingMode addressingMode) => SubtractWithCarryHandler(addressingMode);

        private AddressingMode GetAddressingMode(byte opcode)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (opcode)
            {
                case 0x6C:
                    return AddressingMode.Indirect;

                case 0x00:
                case 0x08:
                case 0x0A:
                case 0x18:
                case 0x28:
                case 0x2A:
                case 0x38:
                case 0x40:
                case 0x48:
                case 0x4A:
                case 0x58:
                case 0x60:
                case 0x68:
                case 0x6A:
                case 0x78:
                case 0x88:
                case 0x8A:
                case 0x98:
                case 0x9A:
                case 0xA8:
                case 0xAA:
                case 0xB8:
                case 0xBA:
                case 0xC8:
                case 0xCA:
                case 0xD8:
                case 0xE8:
                case 0xEA:
                case 0xF8:
                    return AddressingMode.Implied;

                case 0x01:
                case 0x21:
                case 0x41:
                case 0x61:
                case 0x81:
                case 0xA1:
                case 0xC1:
                case 0xE1:
                    return AddressingMode.IndexedIndirect;

                case 0x05:
                case 0x06:
                case 0x24:
                case 0x25:
                case 0x26:
                case 0x45:
                case 0x46:
                case 0x65:
                case 0x66:
                case 0x84:
                case 0x85:
                case 0x86:
                case 0xA4:
                case 0xA5:
                case 0xA6:
                case 0xC4:
                case 0xC5:
                case 0xC6:
                case 0xE4:
                case 0xE5:
                case 0xE6:
                    return AddressingMode.ZeroPage;

                case 0x09:
                case 0x29:
                case 0x49:
                case 0x69:
                case 0xA0:
                case 0xA2:
                case 0xA9:
                case 0xC0:
                case 0xC9:
                case 0xE0:
                case 0xE9:
                    return AddressingMode.Immediate;

                case 0x0D:
                case 0x0E:
                case 0x20:
                case 0x2C:
                case 0x2D:
                case 0x2E:
                case 0x4C:
                case 0x4D:
                case 0x4E:
                case 0x6D:
                case 0x6E:
                case 0x8C:
                case 0x8D:
                case 0x8E:
                case 0xAC:
                case 0xAD:
                case 0xAE:
                case 0xCC:
                case 0xCD:
                case 0xCE:
                case 0xEC:
                case 0xED:
                case 0xEE:
                    return AddressingMode.Absolute;

                case 0x10:
                case 0x30:
                case 0x50:
                case 0x70:
                case 0x90:
                case 0xB0:
                case 0xD0:
                case 0xF0:
                    return AddressingMode.Relative;

                case 0x11:
                case 0x31:
                case 0x51:
                case 0x71:
                case 0x91:
                case 0xB1:
                case 0xD1:
                case 0xF1:
                    return AddressingMode.IndirectIndexed;

                case 0x15:
                case 0x16:
                case 0x35:
                case 0x36:
                case 0x55:
                case 0x56:
                case 0x75:
                case 0x76:
                case 0x94:
                case 0x95:
                case 0xB4:
                case 0xB5:
                case 0xD5:
                case 0xD6:
                case 0xF5:
                case 0xF6:
                    return AddressingMode.ZeroPageX;

                case 0x19:
                case 0x39:
                case 0x59:
                case 0x79:
                case 0x99:
                case 0xB9:
                case 0xBE:
                case 0xD9:
                case 0xF9:
                    return AddressingMode.AbsoluteY;

                case 0x1D:
                case 0x1E:
                case 0x3D:
                case 0x3E:
                case 0x5D:
                case 0x5E:
                case 0x7D:
                case 0x7E:
                case 0x9D:
                case 0xBC:
                case 0xBD:
                case 0xDD:
                case 0xDE:
                case 0xFD:
                case 0xFE:
                    return AddressingMode.AbsoluteX;

                case 0x96:
                case 0xB6:
                    return AddressingMode.ZeroPageY;

            }

            throw new NotImplementedException();
        }

        private Action<AddressingMode> GetHandler(byte opcode)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (opcode)
            {
                case 0x00: return Brk;
                case 0x08: return Php;
                case 0x10: return Bpl;
                case 0x18: return Clc;
                case 0x20: return Jsr;
                case 0x28: return Plp;
                case 0x30: return Bmi;
                case 0x38: return Sec;
                case 0x40: return Rti;
                case 0x48: return Pha;
                case 0x50: return Bvc;
                case 0x58: return Cli;
                case 0x60: return Rts;
                case 0x68: return Pla;
                case 0x70: return Bvs;
                case 0x78: return Sei;
                case 0x88: return Dey;
                case 0x8A: return Txa;
                case 0x90: return Bcc;
                case 0x98: return Tya;
                case 0x9A: return Txs;
                case 0xA8: return Tay;
                case 0xAA: return Tax;
                case 0xB0: return Bcs;
                case 0xB8: return Clv;
                case 0xBA: return Tsx;
                case 0xC8: return Iny;
                case 0xCA: return Dex;
                case 0xD0: return Bne;
                case 0xD8: return Cld;
                case 0xE8: return Inx;
                case 0xEA: return Nop;
                case 0xF0: return Beq;
                case 0xF8: return Sed;

                case 0x01:
                case 0x05:
                case 0x09:
                case 0x0D:
                case 0x11:
                case 0x15:
                case 0x19:
                case 0x1D:
                    return Ora;

                case 0x06:
                case 0x0A:
                case 0x0E:
                case 0x16:
                case 0x1E:
                    return Asl;

                case 0x21:
                case 0x25:
                case 0x29:
                case 0x2D:
                case 0x31:
                case 0x35:
                case 0x39:
                case 0x3D:
                    return And;

                case 0x24:
                case 0x2C:
                    return Bit;

                case 0x26:
                case 0x2A:
                case 0x2E:
                case 0x36:
                case 0x3E:
                    return Rol;

                case 0x41:
                case 0x45:
                case 0x49:
                case 0x4D:
                case 0x51:
                case 0x55:
                case 0x59:
                case 0x5D:
                    return Eor;

                case 0x46:
                case 0x4A:
                case 0x4E:
                case 0x56:
                case 0x5E:
                    return Lsr;

                case 0x4C:
                case 0x6C:
                    return Jmp;

                case 0x61:
                case 0x65:
                case 0x69:
                case 0x6D:
                case 0x71:
                case 0x75:
                case 0x79:
                case 0x7D:
                    return Adc;

                case 0x66:
                case 0x6A:
                case 0x6E:
                case 0x76:
                case 0x7E:
                    return Ror;

                case 0x81:
                case 0x85:
                case 0x8D:
                case 0x91:
                case 0x95:
                case 0x99:
                case 0x9D:
                    return Sta;

                case 0x84:
                case 0x8C:
                case 0x94:
                    return Sty;

                case 0x86:
                case 0x8E:
                case 0x96:
                    return Stx;

                case 0xA0:
                case 0xA4:
                case 0xAC:
                case 0xB4:
                case 0xBC:
                    return Ldy;

                case 0xA1:
                case 0xA5:
                case 0xA9:
                case 0xAD:
                case 0xB1:
                case 0xB5:
                case 0xB9:
                case 0xBD:
                    return Lda;

                case 0xA2:
                case 0xA6:
                case 0xAE:
                case 0xB6:
                case 0xBE:
                    return Ldx;

                case 0xC0:
                case 0xC4:
                case 0xCC:
                    return Cpy;

                case 0xC1:
                case 0xC5:
                case 0xC9:
                case 0xCD:
                case 0xD1:
                case 0xD5:
                case 0xD9:
                case 0xDD:
                    return Cmp;

                case 0xC6:
                case 0xCE:
                case 0xD6:
                case 0xDE:
                    return Dec;

                case 0xE0:
                case 0xE4:
                case 0xEC:
                    return Cpx;

                case 0xE1:
                case 0xE5:
                case 0xE9:
                case 0xED:
                case 0xF1:
                case 0xF5:
                case 0xF9:
                case 0xFD:
                    return Sbc;

                case 0xE6:
                case 0xEE:
                case 0xF6:
                case 0xFE:
                    return Inc;

            }

            throw new NotImplementedException();
        }

        private string GetMnemonic(byte opcode)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (opcode)
            {
                case 0x00: return "BRK";
                case 0x08: return "PHP";
                case 0x10: return "BPL";
                case 0x18: return "CLC";
                case 0x20: return "JSR";
                case 0x28: return "PLP";
                case 0x30: return "BMI";
                case 0x38: return "SEC";
                case 0x40: return "RTI";
                case 0x48: return "PHA";
                case 0x50: return "BVC";
                case 0x58: return "CLI";
                case 0x60: return "RTS";
                case 0x68: return "PLA";
                case 0x70: return "BVS";
                case 0x78: return "SEI";
                case 0x88: return "DEY";
                case 0x8A: return "TXA";
                case 0x90: return "BCC";
                case 0x98: return "TYA";
                case 0x9A: return "TXS";
                case 0xA8: return "TAY";
                case 0xAA: return "TAX";
                case 0xB0: return "BCS";
                case 0xB8: return "CLV";
                case 0xBA: return "TSX";
                case 0xC8: return "INY";
                case 0xCA: return "DEX";
                case 0xD0: return "BNE";
                case 0xD8: return "CLD";
                case 0xE8: return "INX";
                case 0xEA: return "NOP";
                case 0xF0: return "BEQ";
                case 0xF8: return "SED";

                case 0x01:
                case 0x05:
                case 0x09:
                case 0x0D:
                case 0x11:
                case 0x15:
                case 0x19:
                case 0x1D:
                    return "ORA";

                case 0x06:
                case 0x0A:
                case 0x0E:
                case 0x16:
                case 0x1E:
                    return "ASL";

                case 0x21:
                case 0x25:
                case 0x29:
                case 0x2D:
                case 0x31:
                case 0x35:
                case 0x39:
                case 0x3D:
                    return "AND";

                case 0x24:
                case 0x2C:
                    return "BIT";

                case 0x26:
                case 0x2A:
                case 0x2E:
                case 0x36:
                case 0x3E:
                    return "ROL";

                case 0x41:
                case 0x45:
                case 0x49:
                case 0x4D:
                case 0x51:
                case 0x55:
                case 0x59:
                case 0x5D:
                    return "EOR";

                case 0x46:
                case 0x4A:
                case 0x4E:
                case 0x56:
                case 0x5E:
                    return "LSR";

                case 0x4C:
                case 0x6C:
                    return "JMP";

                case 0x61:
                case 0x65:
                case 0x69:
                case 0x6D:
                case 0x71:
                case 0x75:
                case 0x79:
                case 0x7D:
                    return "ADC";

                case 0x66:
                case 0x6A:
                case 0x6E:
                case 0x76:
                case 0x7E:
                    return "ROR";

                case 0x81:
                case 0x85:
                case 0x8D:
                case 0x91:
                case 0x95:
                case 0x99:
                case 0x9D:
                    return "STA";

                case 0x84:
                case 0x8C:
                case 0x94:
                    return "STY";

                case 0x86:
                case 0x8E:
                case 0x96:
                    return "STX";

                case 0xA0:
                case 0xA4:
                case 0xAC:
                case 0xB4:
                case 0xBC:
                    return "LDY";

                case 0xA1:
                case 0xA5:
                case 0xA9:
                case 0xAD:
                case 0xB1:
                case 0xB5:
                case 0xB9:
                case 0xBD:
                    return "LDA";

                case 0xA2:
                case 0xA6:
                case 0xAE:
                case 0xB6:
                case 0xBE:
                    return "LDX";

                case 0xC0:
                case 0xC4:
                case 0xCC:
                    return "CPY";

                case 0xC1:
                case 0xC5:
                case 0xC9:
                case 0xCD:
                case 0xD1:
                case 0xD5:
                case 0xD9:
                case 0xDD:
                    return "CMP";

                case 0xC6:
                case 0xCE:
                case 0xD6:
                case 0xDE:
                    return "DEC";

                case 0xE0:
                case 0xE4:
                case 0xEC:
                    return "CPX";

                case 0xE1:
                case 0xE5:
                case 0xE9:
                case 0xED:
                case 0xF1:
                case 0xF5:
                case 0xF9:
                case 0xFD:
                    return "SBC";

                case 0xE6:
                case 0xEE:
                case 0xF6:
                case 0xFE:
                    return "INC";

            }

            throw new NotImplementedException();
        }

        private int GetCycleCount(byte opcode)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (opcode)
            {
                case 0x02:
                case 0x12:
                case 0x22:
                case 0x32:
                case 0x42:
                case 0x52:
                case 0x62:
                case 0x72:
                case 0x92:
                case 0xB2:
                case 0xD2:
                case 0xF2:
                    return 0;

                case 0x09:
                case 0x0A:
                case 0x0B:
                case 0x10:
                case 0x18:
                case 0x1A:
                case 0x29:
                case 0x2A:
                case 0x2B:
                case 0x30:
                case 0x38:
                case 0x3A:
                case 0x49:
                case 0x4A:
                case 0x4B:
                case 0x50:
                case 0x58:
                case 0x5A:
                case 0x69:
                case 0x6A:
                case 0x6B:
                case 0x70:
                case 0x78:
                case 0x7A:
                case 0x80:
                case 0x82:
                case 0x88:
                case 0x89:
                case 0x8A:
                case 0x8B:
                case 0x90:
                case 0x98:
                case 0x9A:
                case 0xA0:
                case 0xA2:
                case 0xA8:
                case 0xA9:
                case 0xAA:
                case 0xAB:
                case 0xB0:
                case 0xB8:
                case 0xBA:
                case 0xC0:
                case 0xC2:
                case 0xC8:
                case 0xC9:
                case 0xCA:
                case 0xCB:
                case 0xD0:
                case 0xD8:
                case 0xDA:
                case 0xE0:
                case 0xE2:
                case 0xE8:
                case 0xE9:
                case 0xEA:
                case 0xEB:
                case 0xF0:
                case 0xF8:
                case 0xFA:
                    return 2;

                case 0x04:
                case 0x05:
                case 0x08:
                case 0x24:
                case 0x25:
                case 0x44:
                case 0x45:
                case 0x48:
                case 0x4C:
                case 0x64:
                case 0x65:
                case 0x84:
                case 0x85:
                case 0x86:
                case 0x87:
                case 0xA4:
                case 0xA5:
                case 0xA6:
                case 0xA7:
                case 0xC4:
                case 0xC5:
                case 0xE4:
                case 0xE5:
                    return 3;

                case 0x0C:
                case 0x0D:
                case 0x14:
                case 0x15:
                case 0x19:
                case 0x1C:
                case 0x1D:
                case 0x28:
                case 0x2C:
                case 0x2D:
                case 0x34:
                case 0x35:
                case 0x39:
                case 0x3C:
                case 0x3D:
                case 0x4D:
                case 0x54:
                case 0x55:
                case 0x59:
                case 0x5C:
                case 0x5D:
                case 0x68:
                case 0x6D:
                case 0x74:
                case 0x75:
                case 0x79:
                case 0x7C:
                case 0x7D:
                case 0x8C:
                case 0x8D:
                case 0x8E:
                case 0x8F:
                case 0x94:
                case 0x95:
                case 0x96:
                case 0x97:
                case 0xAC:
                case 0xAD:
                case 0xAE:
                case 0xAF:
                case 0xB4:
                case 0xB5:
                case 0xB6:
                case 0xB7:
                case 0xB9:
                case 0xBB:
                case 0xBC:
                case 0xBD:
                case 0xBE:
                case 0xBF:
                case 0xCC:
                case 0xCD:
                case 0xD4:
                case 0xD5:
                case 0xD9:
                case 0xDC:
                case 0xDD:
                case 0xEC:
                case 0xED:
                case 0xF4:
                case 0xF5:
                case 0xF9:
                case 0xFC:
                case 0xFD:
                    return 4;

                case 0x06:
                case 0x07:
                case 0x11:
                case 0x26:
                case 0x27:
                case 0x31:
                case 0x46:
                case 0x47:
                case 0x51:
                case 0x66:
                case 0x67:
                case 0x6C:
                case 0x71:
                case 0x99:
                case 0x9B:
                case 0x9C:
                case 0x9D:
                case 0x9E:
                case 0x9F:
                case 0xB1:
                case 0xB3:
                case 0xC6:
                case 0xC7:
                case 0xD1:
                case 0xE6:
                case 0xE7:
                case 0xF1:
                    return 5;

                case 0x01:
                case 0x0E:
                case 0x0F:
                case 0x16:
                case 0x17:
                case 0x20:
                case 0x21:
                case 0x2E:
                case 0x2F:
                case 0x36:
                case 0x37:
                case 0x40:
                case 0x41:
                case 0x4E:
                case 0x4F:
                case 0x56:
                case 0x57:
                case 0x60:
                case 0x61:
                case 0x6E:
                case 0x6F:
                case 0x76:
                case 0x77:
                case 0x81:
                case 0x83:
                case 0x91:
                case 0x93:
                case 0xA1:
                case 0xA3:
                case 0xC1:
                case 0xCE:
                case 0xCF:
                case 0xD6:
                case 0xD7:
                case 0xE1:
                case 0xEE:
                case 0xEF:
                case 0xF6:
                case 0xF7:
                    return 6;

                case 0x00:
                case 0x1B:
                case 0x1E:
                case 0x1F:
                case 0x3B:
                case 0x3E:
                case 0x3F:
                case 0x5B:
                case 0x5E:
                case 0x5F:
                case 0x7B:
                case 0x7E:
                case 0x7F:
                case 0xDB:
                case 0xDE:
                case 0xDF:
                case 0xFB:
                case 0xFE:
                case 0xFF:
                    return 7;

                case 0x03:
                case 0x13:
                case 0x23:
                case 0x33:
                case 0x43:
                case 0x53:
                case 0x63:
                case 0x73:
                case 0xC3:
                case 0xD3:
                case 0xE3:
                case 0xF3:
                    return 8;

            }

            throw new NotImplementedException();
        }
    }
}
