﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using nes_emulator.Mappers;

namespace nes_emulator
{
    internal enum MirroringMode
    {
        Vertical,
        Horizontal
    }

    internal struct NesHeader
    {
        public byte programRomSize;
        public byte programRamSize;
        public byte characterRomSize;
        public byte flags6;
        public byte flags7;
        public byte flags9;
        public byte flags10;

        public NesHeader(IReadOnlyList<byte> data)
        {
            programRomSize = data[4];
            programRamSize = data[8];
            characterRomSize = data[5];
            flags6 = data[6];
            flags7 = data[7];
            flags9 = data[9];
            flags10 = data[10];
        }

        public byte Mapper()
        {
            return (byte)((flags7 & 0xF0) | flags6 >> 4);
        }

        public bool Trainer()
        {
            return (flags6 & 0x04) != 0;
        }
    }

    internal class Cartridge
    {
        private readonly IMapper _mapper;

        private readonly byte[] _programRom;
        private readonly byte[] _characterRom;

        public MirroringMode MirroringMode { get; private set; }

        public Cartridge(string romPath)
        {
            try
            {
                var fileBytes = File.ReadAllBytes(romPath);
                var headerBytes = fileBytes.Take(16).ToArray();

                var header = new NesHeader(headerBytes);

                switch (header.Mapper())
                {
                    case 0:
                        _mapper = new Mapper000(header.programRomSize, header.characterRomSize);

                        break;

                    default:
                        throw new Exception($"Unknown mapper type: 0x{header.Mapper():X02}");

                }

                MirroringMode = (header.flags6 & 0x01) > 0 ? MirroringMode.Vertical: MirroringMode.Horizontal;

                _programRom = fileBytes.Skip(16).Take(header.programRomSize * 1024 * 16).ToArray();
                _characterRom = fileBytes.Skip(16 + header.programRomSize * 1024 * 16).Take(header.characterRomSize * 1024 * 8).ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Couldn't instantiate Cartridge class: {ex.Message}");
                Environment.Exit(1);
            }
        }

        public bool CpuRead(ushort address, out byte value)
        {
            if (_mapper.CpuMapRead(address, out var mappedAddress))
            {
                value = _programRom[mappedAddress];

                return true;
            }

            value = 0;

            return false;
        }

        public bool PpuRead(ushort address, out byte value)
        {
            if (_mapper.PpuMapRead(address, out var mappedAddress))
            {
                value = _characterRom[mappedAddress];

                return true;
            }

            value = 0;

            return false;
        }

        public bool CpuWrite(ushort address, byte value)
        {
            if (!_mapper.CpuMapWrite(address, out var mappedAddress, value))
                return false;

            _programRom[mappedAddress] = value;

            return true;
        }

        public bool PpuWrite(ushort address, byte value)
        {
            if (!_mapper.PpuMapWrite(address, out var mappedAddress, value))
                return false;

            _characterRom[mappedAddress] = value;

            return true;
        }

        public void Reset()
        {
            _mapper.Reset();
        }
    }
}
