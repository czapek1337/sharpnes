﻿using System;

namespace nes_emulator.CPU
{
    internal enum ProcessorFlag
    {
        Carry = 0,
        Zero = 1,
        InterruptDisable = 2,
        Decimal = 3,
        Break = 4,
        Unused = 5,
        Overflow = 6,
        Negative = 7
    }

    internal class ProcessorFlags
    {
        private readonly bool[] _flags = new bool[8];

        public long LastUpdate { get; private set; }

        public void SetFlag(ProcessorFlag flag, bool value, bool updateLastUpdate = true)
        {
            _flags[(int)flag] = value;

            if (updateLastUpdate)
                LastUpdate = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        }

        public bool GetFlag(ProcessorFlag flag)
        {
            return _flags[(int)flag];
        }

        public byte GetByte()
        {
            return (byte)((byte)(GetFlag(ProcessorFlag.Carry) ? 1 << 0 : 0) |
                          (byte)(GetFlag(ProcessorFlag.Zero) ? 1 << 1 : 0) |
                          (byte)(GetFlag(ProcessorFlag.InterruptDisable) ? 1 << 2 : 0) |
                          (byte)(GetFlag(ProcessorFlag.Decimal) ? 1 << 3 : 0) |
                          (byte)(GetFlag(ProcessorFlag.Break) ? 1 << 4 : 0) |
                          (byte)(GetFlag(ProcessorFlag.Unused) ? 1 << 5 : 0) |
                          (byte)(GetFlag(ProcessorFlag.Overflow) ? 1 << 6 : 0) |
                          (byte)(GetFlag(ProcessorFlag.Negative) ? 1 << 7 : 0));
        }

        public void SetByte(byte data)
        {
            SetFlag(ProcessorFlag.Carry, (data & (1 << 0)) > 0);
            SetFlag(ProcessorFlag.Zero, (data & (1 << 1)) > 0, false);
            SetFlag(ProcessorFlag.InterruptDisable, (data & (1 << 2)) > 0, false);
            SetFlag(ProcessorFlag.Decimal, (data & (1 << 3)) > 0, false);
            SetFlag(ProcessorFlag.Break, false, false);
            SetFlag(ProcessorFlag.Unused, true, false);
            SetFlag(ProcessorFlag.Overflow, (data & (1 << 6)) > 0, false);
            SetFlag(ProcessorFlag.Negative, (data & (1 << 7)) > 0, false);
        }
    }
}
