﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nes_emulator.CPU
{
    internal enum AddressingMode
    {
        Implied,
        Immediate,
        ZeroPage,
        ZeroPageX,
        ZeroPageY,
        Relative,
        Absolute,
        AbsoluteX,
        AbsoluteY,
        Indirect,
        IndexedIndirect,
        IndirectIndexed,
    }

    // ReSharper disable MemberCanBeMadeStatic.Local
    internal partial class Processor
    {
        private static int GetBytesNeeded(AddressingMode addressingMode)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (addressingMode)
            {
                case AddressingMode.Implied:
                    return 0;

                case AddressingMode.Immediate:
                    return 1;

                case AddressingMode.ZeroPage:
                    return 1;

                case AddressingMode.ZeroPageX:
                    return 1;

                case AddressingMode.ZeroPageY:
                    return 1;

                case AddressingMode.Relative:
                    return 1;

                case AddressingMode.Absolute:
                    return 2;

                case AddressingMode.AbsoluteX:
                    return 2;

                case AddressingMode.AbsoluteY:
                    return 2;

                case AddressingMode.Indirect:
                    return 2;

                case AddressingMode.IndexedIndirect:
                    return 1;

                case AddressingMode.IndirectIndexed:
                    return 1;

            }

            throw new NotImplementedException();
        }

        private ushort ResolveAddress(AddressingMode addressingMode)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (addressingMode)
            {
                case AddressingMode.Implied:
                    return 0;
                
                case AddressingMode.Immediate:
                    return ProgramCounter;

                case AddressingMode.ZeroPage:
                    return Read(ProgramCounter);

                case AddressingMode.ZeroPageX:
                    return (ushort) (Read(ProgramCounter) + RegisterX);

                case AddressingMode.ZeroPageY:
                    return (ushort)(Read(ProgramCounter) + RegisterY);

                case AddressingMode.Absolute:
                    return (ushort) (Read(ProgramCounter) | (ushort) (Read((ushort) (ProgramCounter + 1)) << 8));

                case AddressingMode.Relative:
                    var displacement = Read(ProgramCounter) + 1;

                    return (ushort) ((displacement & 0x80) > 0 ? displacement | 0xFF00 : displacement);

                case AddressingMode.AbsoluteX:
                    return (ushort)(Read(ProgramCounter) | (ushort)(Read((ushort)(ProgramCounter + 1)) << 8) + RegisterX);

                case AddressingMode.AbsoluteY:
                    return (ushort)(Read(ProgramCounter) | (ushort)(Read((ushort)(ProgramCounter + 1)) << 8) + RegisterY);

                case AddressingMode.Indirect:
                    var indirectAddress = (ushort) (Read(ProgramCounter) | (ushort) (Read((ushort) (ProgramCounter + 1)) << 8));

                    return (ushort) (Read(indirectAddress) | (ushort) (Read((ushort) (indirectAddress + 1)) << 8));

                case AddressingMode.IndexedIndirect:
                    var offsetX = Read(ProgramCounter);

                    return (ushort) (Read((ushort) (offsetX + RegisterX)) | (ushort) (Read((ushort) (offsetX + RegisterX + 1)) << 8));

                case AddressingMode.IndirectIndexed:
                    var offsetY = Read(ProgramCounter);

                    return (ushort)(Read(offsetY) | (ushort)(Read((ushort)(offsetY + 1)) << 8) + RegisterY);

            }

            return 0;
        }
    }
}
